package fr.view;

import java.util.Scanner;
import fr.control.GeneratePDF;
import fr.metier.Request;

public class Display {

	/**
	 * METHODE D'AFFICHAGE
	 */
	public static void terminal() {

		Scanner in = new Scanner(System.in);
		String str = "";

		while (str.contentEquals("Q") != true) {
			System.out.println();

			System.out.println(
					"-------------------------------         MENU         ------------------------------------------------");
			System.out.println("");
			System.out.println("1 - Cr�er une agence");
			System.out.println("2 - Cr�er un client");
			System.out.println("3 - Cr�er un compte bancaire");
			System.out.println("4 - Rechercher un compte (via son num�ro de compte)");
			System.out.println("5 - Rechercher un client (via son nom, num�ro de compte ou identifiant)");
			System.out.println("6 - Afficher la liste des comptes d�un client (identifiant client)");
			System.out.println("7 - Imprimer les infos client (identifiant client)");
			System.out.println("8 - D�sactiver un client");
			System.out.println("9 - Supprimer un compte");
			System.out.println();
			System.out.println("Q - Quitter");
			System.out.println();
			System.out.println(
					"-----------------------------------------------------------------------------------------------------");
			System.out.println();

			System.out.println("Veuillez renseigner votre choix :");
			str = in.nextLine().toUpperCase();
			System.out.println();

			switch (str) {

			case "1":
				Request.createAgency();
				break;

			case "2":
				Request.createClient();
				break;

			case "3":
				Request.createAccount();
				break;

			case "4":
				Request.displayAccount();
				break;

			case "5":
				Request.displayClient();
				break;

			case "6":
				Request.displayClientAccounts();
				break;

			case "7":
				GeneratePDF.relevePdf();
				break;

			case "8":
				Request.suspendClient();
				break;

			case "9":
				Request.deleteAccount();
				break;

			case "Q":
				System.out.println();
				System.out.println("Merci de votre visite, � bientot!");
				System.out.println();
				break;

			}
		}
	}

}
