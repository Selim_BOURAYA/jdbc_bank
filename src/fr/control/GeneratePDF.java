package fr.control;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.Scanner;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import fr.view.Display;
import junit.framework.Test;

public class GeneratePDF {

	public static final String DEST = "C:/ENV/Documents/releve";
	static int count = 0;
	

	/**
	 * METHODE DE GENERATION DES RELEVES BANCAIRES
	 */
	public static void relevePdf() {

		try {

			Scanner in = new Scanner(System.in);
			count++;
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(DEST + "_" + count + ".pdf"));
			document.open();

			Connection conn = null;
			Statement stServices = null;
			ResultSet listeServices = null;

			ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
			String url = bundle.getString("url");
			String user = bundle.getString("user");
			String password = bundle.getString("password");

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez l'identifiant du client dont vous souhaitez imprimer le relev�");
			String idClient = in.nextLine();

			String strQuery = "select * from clt c inner join acc a on c.idclient = a.idclient inner join estab e on e.idagency = a.idagency where c.idclient = '"
					+ idClient + "'";
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);

			while (listeServices.next()) {

				String line1 = "fiche Client " + "\n\n\n";

				String line2 = "Num�ro client : " + listeServices.getString("idclient") + "\n\n";
				String line3 = "Nom : " + listeServices.getString("firstname") + "\n\n";
				String line4 = "Pr�nom : " + listeServices.getString("lastname") + "\n\n";
				String line5 = "Date de naissance : " + listeServices.getDate("birthdate") + "\n\n";

				String line6 = "______________________________________________________________________________";
				String line7 = "Liste de compte ";
				String line8 = "______________________________________________________________________________";
				String line9 = "Numero de compte                  Solde" + "\n\n";

				Paragraph para4 = new Paragraph();
				para4.setAlignment(Element.ALIGN_LEFT);

				while (listeServices.next()) {
					para4.add("" + listeServices.getString("idaccount") + "                          "
							+ listeServices.getInt("balance") + " euros                                              "
							+ (listeServices.getInt("balance") > 0 ? ":-)" : ":-(") + "\n");
				}

				Paragraph para1 = new Paragraph();
				para1.setAlignment(Element.ALIGN_CENTER);
				para1.add(line1);

				Paragraph para2 = new Paragraph();
				para2.setAlignment(Element.ALIGN_LEFT);
				para2.add(line2);
				para2.add(line3);
				para2.add(line4);
				para2.add(line5);

				Paragraph para3 = new Paragraph();
				para3.setAlignment(Element.ALIGN_LEFT);
				para3.add(line6);
				para3.add(line7);
				para3.add(line8);
				para3.add(line9);

				document.add(para1);
				document.add(para2);
				document.add(para3);
				document.add(para4);
				document.newPage();

			}

			document.close();
			System.out.println("Le relev� a bien �t� g�n�r� en pdf (dossier C:/ENV/Documents/)");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("les informations ne sont pas disponibles");
			Display.terminal();
		}

	}
}
