package fr.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class SQL {

//	VARIABLES
	static Connection conn = null;
	static Statement stServices = null;
	static ResultSet listeServices = null;

	/*
	 * METHODE DE COONECTION A LA BDD
	 */
	public static void connectSQL() {

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
