package fr.metier;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Request {

	/*
	 * METHODE DE CREATION D'AGENCE
	 */
	public static void createAgency()  {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez le nom de l'agence que vous souhaitez ajouter ? ");
			String nameAgency = in.nextLine();
			System.out.println("veuillez en renseigner l'adresse");
			String adress = in.nextLine();

			String strQuery = "insert into estab values (nextval('nextagency'), '" + nameAgency + "', '" + adress
					+ "');";
			stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			System.out.println();
			System.out.println("Le compte a bien �t� cr�e");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	/*
	 * METHODE DE CREATION DE CLIENT
	 */
	public static void createClient() {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez le nom du client que vous souhaitez ajouter ");
			String firstname = in.nextLine();
			System.out.println("Indiquez le prenom du client que vous souhaitez ajouter ");
			String lastname = in.nextLine();
			System.out.println("veuillez renseigner la date de naissance");
			String birthdate = in.nextLine();
			System.out.println("veuillez renseigner l'email");
			String email = in.nextLine();

			String strQuery = "insert into clt values (nextval('nextclient'), '" + firstname + "', '" + lastname
					+ "', '" + birthdate + "', '" + email + "')";
			stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			System.out.println();
			System.out.println("Le client a bien �t� cr�e");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	/*
	 * METHODE DE CREATION DES COMPTES
	 */
	public static void createAccount() {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez l'identifiant de la banque auquel le compte est rattache");
			String idAgency = in.nextLine();
			System.out.println("Indiquez l'identifiant du client auquel vous souhaitez lier le compte");
			String idClient = in.nextLine();
			System.out.println("veuillez renseigner le montant des fonds � transferer sur le compte");
			String amount = in.nextLine();
			System.out.println("souhaitez vous interdire le d�couvert autoris� ? (O/N)");
			String active = in.nextLine();

			if (active.contentEquals("O") == true) {
				String strQuery = "insert into acc values (nextval('nextaccount'), " + idAgency + ", " + idClient + ", "
						+ amount + ")";
				stServices = conn.createStatement();
				stServices.executeUpdate(strQuery);
			}

			else {
				String strQuery = "insert into acc values (nextval('nextaccount'), " + idAgency + ", " + idClient + ", "+ amount + ", false)";
				stServices = conn.createStatement();
				stServices.executeUpdate(strQuery);
				System.out.println();
				System.out.println("Le compte a bien �t� cr�e");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	/*
	 * METHODE DE VISUALISATION DES COMPTES CLIENT
	 */
	public static void displayAccount() {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez l'identifiant du compte � afficher");
			String idaccount = in.nextLine();

			String strQuery = "select a.idaccount, e.nameagency, c.firstname, a.balance from acc a inner join estab e on e.idagency = a.idagency inner join clt c on a.idclient =  c.idclient where idaccount ="
					+ idaccount + ";";

			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);

			while (listeServices.next()) {

				System.out.println();
				System.out.println("Account n� : " + listeServices.getInt("idaccount"));
				System.out.println("Agency of " + listeServices.getString("nameagency"));
				System.out.println("owned by M/Mrs : " + listeServices.getString("firstname"));
				System.out.println("current balance : " + listeServices.getInt("balance"));

				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	/*
	 * METHODE DE VISUALISATION DES INFORMATIONS CLIENT
	 */
	public static void displayClient() {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			String str = "";
			String strQuery = "";
			String userChoice = "";

			while (str.contentEquals("Q") != true) {
				System.out.println();
				System.out.println(
						"-------------------------------         MENU         ------------------------------------------------");
				System.out.println("");
				System.out.println("1 - visualiser les informations client gr�ce � son nom");
				System.out.println("2 - visualiser les informations client gr�ce � son numero de compte");
				System.out.println("3 - visualiser les informations client gr�ce � son identifiant");
				System.out.println();
				System.out.println("Q - Quitter");
				System.out.println();
				System.out.println(
						"-----------------------------------------------------------------------------------------------------");
				System.out.println();

				System.out.println("Veuillez renseigner votre choix :");
				str = in.nextLine().toUpperCase();
				System.out.println();

				switch (str) {

				case "1":

					System.out.println("Indiquez le nom du client dont vous souhaitez consulter les informations");
					userChoice = in.nextLine();

					strQuery = "select c.firstname, c.lastname, c.birthdate, c.email, c.idclient from clt c where c.firstname ='"
							+ userChoice + "'";
					stServices = conn.createStatement();
					listeServices = stServices.executeQuery(strQuery);

					while (listeServices.next()) {

						System.out.println();
						System.out.println("M/Mrs : " + listeServices.getString("firstname") + " "
								+ listeServices.getString("lastname"));
						System.out.println("birthdate :  " + listeServices.getDate("birthdate"));
						System.out.println("email : " + listeServices.getString("email"));
						System.out.println("id client : " + listeServices.getString("idclient"));

						System.out.println("---------------------------------------------------------");
					}
					break;

				case "2":

					System.out.println(
							"Indiquez le num�ro de compte du client dont vous souhaitez consulter les comptes bancaires ");
					userChoice = in.nextLine();

					strQuery = "select c.firstname, c.lastname, c.birthdate, c.email, c.idclient from clt c inner join acc a on c.idclient = a.idclient where a.idaccount ="
							+ userChoice + "";
					stServices = conn.createStatement();
					listeServices = stServices.executeQuery(strQuery);

					while (listeServices.next()) {

						System.out.println();
						System.out.println("M/Mrs : " + listeServices.getString("firstname"));
						System.out.println(listeServices.getString("lastname"));
						System.out.println("birthdate :  " + listeServices.getDate("birthdate"));
						System.out.println("email : " + listeServices.getString("email"));
						System.out.println("id client : " + listeServices.getString("idclient"));

						System.out.println("---------------------------------------------------------");
					}
					break;

				case "3":

					System.out.println(
							"Indiquez l'identifiant du client dont vous souhaitez consulter les comptes bancaires ");
					userChoice = in.nextLine();

					strQuery = "select c.firstname, c.lastname, c.birthdate, c.email, c.idclient from clt c inner join acc a on c.idclient = a.ididclient where c.idclient ="
							+ userChoice + "";

					stServices = conn.createStatement();
					listeServices = stServices.executeQuery(strQuery);

					while (listeServices.next()) {

						System.out.println();
						System.out.println("M/Mrs : " + listeServices.getString("firstname"));
						System.out.println(listeServices.getString("lastname"));
						System.out.println("birthdate :  " + listeServices.getDate("birthdate"));
						System.out.println("email : " + listeServices.getString("email"));
						System.out.println("id client : " + listeServices.getString("idclient"));

						System.out.println("---------------------------------------------------------");
					}

					break;

				}
				break;
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}

	/*
	 * METHODE DE VISUALISATION DES COMPTES DE CLIENT
	 */
	public static void displayClientAccounts() {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez l'identifiant du client dont vous souhaitez consulter les comptes");
			String idclient = in.nextLine();

			String strQuery = "select a.idaccount, e.nameagency, c.firstname, a.balance from acc a inner join estab e on e.idagency = a.idagency inner join clt c on a.idclient =  c.idclient where c.idclient ='"
					+ idclient + "' order by a.idaccount asc;";

			stServices = conn.createStatement();

			listeServices = stServices.executeQuery(strQuery);

			while (listeServices.next()) {

				System.out.println();
				System.out.println("Account n� : " + listeServices.getBigDecimal("idaccount"));
				System.out.println("Agency of " + listeServices.getString("nameagency"));
				System.out.println("owned by M/Mrs : " + listeServices.getString("firstname"));
				System.out.println("current balance : " + listeServices.getInt("balance"));

				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	/*
	 * METHODE DE SUPPRESSION DE COMPTE
	 */
	public static void deleteAccount() {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez l'identifiant du compte � afficher");
			String idaccount = in.nextLine();

			String strQuery = "delete from acc where idaccount =" + idaccount + ";";

			stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);
			System.out.println();
			System.out.println("Le compte " + idaccount + " est supprim�");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	/*
	 * METHODE DE SUSPENSION DE COMPTE CLIENT
	 */
	public static void suspendClient() {

		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;

		ResourceBundle bundle = ResourceBundle.getBundle("connection.properties.config");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String password = bundle.getString("password");

		try {

			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);

			conn = DriverManager.getConnection(url, user, password);

			System.out.println("Indiquez l'identifiant du client dont vous souhaitez suspendre le compte utilisateur");
			String idClient = in.nextLine();

			String strQuery = "UPDATE clt SET active = false WHERE idclient = '" + idClient + "'";

			stServices = conn.createStatement();
			stServices.executeUpdate(strQuery);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
